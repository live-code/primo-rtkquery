import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {deleteProduct, getProducts} from "./store/products.actions";
import {selectProducts, selectProductsFailed} from "./store/products.store";

export const CatalogPage: React.VFC = props => {
  const dispatch = useDispatch();
  const products = useSelector(selectProducts)
  const error = useSelector(selectProductsFailed)

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])

  return <div>
    {error && <div>error!!!</div>}
    {
      products.map(p => {
        return <li key={p.id}>
          {p.title}
          <button onClick={() => dispatch(deleteProduct(p.id))}>delete</button>
        </li>
      })
    }
  </div>
}

