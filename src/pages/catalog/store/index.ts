import {combineReducers} from "@reduxjs/toolkit";
import {productsStore} from "./products.store";
import {offersStore} from "./offers.store";

export const catalogReducer = combineReducers({
  products: productsStore.reducer,
  offers: offersStore.reducer,
})
