import axios from "axios";
import {AppThunk} from "../../../App";
import {
  deleteProductFailed, deleteProductsStart,
  deleteProductSuccess,
  getProductsFailed, getProductsStart,
  getProductsSuccess,
  Product
} from "./products.store";

export const getProducts = (): AppThunk => dispatch => {
  dispatch(getProductsStart())
  // dispatch(showLoader())
  axios.get<Product[]>('http://localhost:3001/products')
    .then(res => {
      dispatch(getProductsSuccess(res.data))
    })
    .catch(err => {
      dispatch(getProductsFailed())
    })
}


export const deleteProduct = (id: number): AppThunk => dispatch => {
  dispatch(deleteProductsStart())
  axios.delete('http://localhost:3001/products/' + id)
    .then(() => {
      dispatch(deleteProductSuccess(id))
    })
    .catch(err => {
      dispatch(deleteProductFailed())
    })
}

