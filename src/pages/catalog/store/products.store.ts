import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../../App";

export interface Product {
  id: number;
  title: string;
  price: number;
}

export interface Catalog {
  list: Product[];
  error: boolean ;
  pending: boolean;
}

const initialState: Catalog = {
  list: [],
  pending: false,
  error: false,
};

export const productsStore = createSlice({
  name: 'products',
  initialState,
  reducers: {
    getProductsStart(state) {
      state.pending = true;
    },
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.pending = false;
      state.error = false;
      state.list = action.payload
    },
    getProductsFailed(state, action: PayloadAction<void>) {
      state.pending = false;
      state.error = true;
    },
    deleteProductsStart(state) {
      state.pending = true;
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.pending = false;
      state.error = false;
      const index = state.list.findIndex(p  => p.id === action.payload)
      state.list.splice(index, 1);

    },
    deleteProductFailed(state) {
      state.pending = false;
      state.error = true;
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.pending = false;
      state.error = false;
      state.list.push(action.payload);
      // state.list = [...state.list, action.payload]
    }
  }
})

export const {
  getProductsStart,
  getProductsSuccess,
  getProductsFailed,
  deleteProductsStart,
  deleteProductSuccess,
  deleteProductFailed,
  addProductSuccess
} = productsStore.actions



/// SELECTORS

export const selectProducts = (state: RootState) => state.catalog.products.list
export const selectProductsFailed = (state: RootState) => state.catalog.products.error
export const selectProductsPending = (state: RootState) => state.catalog.products.pending
