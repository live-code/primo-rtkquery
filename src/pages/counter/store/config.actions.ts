import {createAction} from "@reduxjs/toolkit";
import {Config} from "./config.reducer";

export const setConfig = createAction<Partial<Config>>('counter/setConfig')
