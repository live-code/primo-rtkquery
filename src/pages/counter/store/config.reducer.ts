import {createReducer} from "@reduxjs/toolkit";
import {setConfig} from "./config.actions";

export interface Config {
  itemsPerPallet: number;
  material: 'wood' | 'plastic'
}


const INITIAL_STATE = { itemsPerPallet: 10, material: 'wood'};

export const configReducer = createReducer(INITIAL_STATE, builder =>
  builder
    .addCase(setConfig, (state, action) => ({ ...state, ...action.payload }))
)
