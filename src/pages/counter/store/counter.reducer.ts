import {createReducer, PayloadAction} from "@reduxjs/toolkit";
import {decrement, increment, reset} from "./counter.actions";

export const counterReducer = createReducer<number>(0, builder =>
  builder
    .addCase(increment, (state, action) => state + action.payload)
    .addCase(decrement, (state, action) => state - action.payload)
    .addCase(reset, () => 0)
)

export const counterReducerManualType = createReducer<number>(0, {
  [increment.type]: (state, action) => state + action.payload,
  'counter/decrement': (state, action: PayloadAction<number>) => state - action.payload
})

export function counterReducerOLD(state = 0, action: any) {
  console.log(state, action)
  switch (action.type) {
    case 'counter/increment':
      return state + action.payload
  }
  return state;
}


