import {useDispatch, useSelector} from "react-redux";
import {increment, reset} from "./store/counter.actions";
import {getCounter, getTotalPallet} from "./store/counter.selectors";
import {setConfig} from "./store/config.actions";

export const CounterPage = () => {
  const dispatch = useDispatch();
  const counter = useSelector(getCounter)
  const pallets = useSelector(getTotalPallet)

  return (
    <div>
      Counter Page {counter} - {pallets}
      <button onClick={() => dispatch(increment(4))}>+</button>
      <button onClick={() => dispatch(reset())}>RESET</button>

      <hr/>
      <button onClick={() => dispatch(setConfig({ itemsPerPallet: 5}))}>
        set pallet to 5
      </button>
      <button onClick={() => dispatch(setConfig({ itemsPerPallet: 10}))}>
        set pallet to 10
      </button>

      <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>
        set pallet plastic
      </button>
      <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>
        set pallet  wood
      </button>
    </div>
  )
}
