import React from 'react';
import {Action, combineReducers, configureStore, ThunkAction} from "@reduxjs/toolkit";
import {Provider} from "react-redux";
import {CounterPage} from "./pages/counter/CounterPage";
import {counterReducer} from "./pages/counter/store/counter.reducer";
import {configReducer} from "./pages/counter/store/config.reducer";
import {CatalogPage} from "./pages/catalog/CatalogPage";
import {productsStore} from "./pages/catalog/store/products.store";
import {offersStore} from "./pages/catalog/store/offers.store";
import {catalogReducer} from "./pages/catalog/store";

const rootReducer = combineReducers({
  catalog: catalogReducer,
  counter: counterReducer,
  config: configReducer
})

export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

function App() {
  return (
    <Provider store={store}>
      {/*<CounterPage />*/}
      <CatalogPage />
    </Provider>
  );
}

export default App;
